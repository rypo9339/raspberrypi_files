
import time
from Sabertooth import Sabertooth

# The Sabertooth is on address 128. We'll name its object ST.
# If you've set up your Sabertooth on a different address, of course change
# that here. For how to configure address, etc. see the DIP Switch Wizard at:
#   http://www.dimensionengineering.com/datasheets/SabertoothDIPWizard/start.htm
# Be sure to select Packetized Serial Mode for use with this library.
# This sample uses a baud rate of 9600.
#
# Replace COM2 with the port you use.
#
# For a USB-to-TTL serial converter from a PC, the connections to make are:
#   TX  (Transmit) -> Sabertooth S1
#   GND (Ground)   -> Sabertooth 0V
#
# For Netduino, the connections to make are:
#   Netduino D3  -> Sabertooth S1
#   Netduino GND -> Sabertooth 0V
#   Netduino 5V  -> Sabertooth 5V (OPTIONAL, if you want the Sabertooth to power the Netduino)
ST = Sabertooth("COM2", 9600)
# Send the autobaud command to the Sabertooth controller(s).
# NOTE: *Not all* Sabertooth controllers need this command.
#       It doesn't hurt anything, but V2 controllers use an
#       EEPROM setting (changeable with the function SetBaudRate) to set
#       the baud rate instead of detecting with autobaud.
#
#       If you have a 2x12, 2x25 V2, 2x60 or SyRen 50, you can remove
#       the autobaud line and save yourself two seconds of startup delay.
ST.AutoBaud()
while True:
    power = 0
    # Ramp throttle from -127 to 127, waiting 20 ms (1/50th of a second) per value.
    for power in range(-127, 128):
        ST.Drive(power)
        time.sleep(0.02)
    # Now go back the way we came.
    for power in range(127, -128, -1):
        ST.Drive(power)
        time.sleep(0.02)
    # Stop driving.
    ST.Drive(0)
    # Ramp turning from -127 to 127, waiting 20 ms (1/50th of a second) per value.
    for power in range(-127, 128):
        ST.Turn(power)
        time.sleep(0.02)
    # Now go back the way we came.
    for power in range(127, -128, -1):
        ST.Turn(power)
        time.sleep(0.02)
    # Stop turning.
    ST.Turn(0)

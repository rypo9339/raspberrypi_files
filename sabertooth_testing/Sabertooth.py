
import time
import serial

class Sabertooth:
    def __init__(self):
        self._address = 128
        self._buffer = bytearray(4)
        self._port = None

    def open(self, port_name, baud_rate):
        self._port = serial.Serial(port_name, baud_rate)

    def close(self):
        if self._port is not None:
            self._port.close()
            self._port = None

    def _require_port(self):
        if self._port is None:
            raise Exception("Serial port is not open")

    def _auto_baud(self, do_not_wait=False):
        if self._port is None:
            raise Exception("Serial port is not open")

        if not do_not_wait:
            time.sleep(1.5)

        self._port.write(b'\xaa')
        self._port.flush()

        if not do_not_wait:
            time.sleep(0.5)

    def auto_baud(self, do_not_wait=False):
        self._require_port()
        self._auto_baud(do_not_wait)

    def command(self, command, value):
        if command < 0 or command > 127:
            raise ValueError("Command value out of range")

        if value < 0 or value > 127:
            raise ValueError("Value out of range")

        self._require_port()

        self._buffer[0] = self._address
        self._buffer[1] = command
        self._buffer[2] = value
        self._buffer[3] = (self._buffer[0] + self._buffer[1] + self._buffer[2]) & 127

        self._port.write(self._buffer)

    def throttle_command(self, command, power):
        power = max(-126, min(126, power))
        self.command(command, abs(power))

    def motor(self, motor_number, power):
        if motor_number < 1:
            raise ValueError("Motor number out of range")

        if motor_number > 2:
            return

        self.throttle_command((4 if motor_number == 2 else 0) + (1 if power < 0 else 0), power)

    def drive(self, power):
        self.throttle_command(9 if power < 0 else 8, power)

    def turn(self, power):
        self.throttle_command(11 if power < 0 else 10, power)

    def stop(self):
        self.motor(1, 0)
        self.motor(2, 0)

    def set_min_voltage(self, value):
        if value < 0 or value > 120:
            raise ValueError("Value out of range")

        self.command(2, value)

    def set_max_voltage(self, value):
        if value < 0 or value > 127:
            raise ValueError("Value out of range")

        self.command(3, value)

    def set_baud_rate(self, baud_rate):
        self._require_port()
        self._port.flush()

        value = None
        if baud_rate == 2400:
            value = 1
        elif baud_rate == 9600:
            value = 2
        elif baud_rate == 19200:
            value = 3
        elif baud_rate == 38400:
            value = 4
        elif baud_rate == 115200:
            value = 5
        else:
            raise ValueError("Unsupported baud rate")

        self.command(15, value)
        self._port.flush()
        time.sleep(0.5)

    def set_deadband(self, value):
        if value < 0 or value > 127:
            raise ValueError("Value out of range")

        self.command(17, value)

    def set_ramping(self, value):
        if value < 0 or value > 80:
            raise ValueError("Value out of range")

        self.command(18, value)

    def set_timeout(self, milliseconds):
        if milliseconds < 0 or milliseconds > 12700:
            raise ValueError("Value out of range")

        self.command(14, (milliseconds + 99) // 100)

    @property
    def address(self):
        return self._address

    @address.setter
    def address(self, value):
        if value < 128 or value > 255:
            raise ValueError("Address value out of range")

        self._address = value




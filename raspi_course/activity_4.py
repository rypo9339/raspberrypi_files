import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

LED_PIN = 17

GPIO.setup(LED_PIN,GPIO.OUT)
GPIO.output(LED_PIN, GPIO.LOW)
i = 0

def LED_ON():
    GPIO.output(LED_PIN, GPIO.HIGH)

def LED_OFF():
    GPIO.output(LED_PIN, GPIO.LOW)

while i == 0:
    command = int(input("Type '1' to turn on the LED\n"))
    if command == 1:
        LED_ON()
        i += 1
        time.sleep(1)
    else:
        print("try again")

while i == 1:
    command = int(input("Type '0' to turn on the LED\n"))
    if command == 0:
        LED_OFF()
        i += 1
    else:
        print("try again")

GPIO.cleanup()

exit()

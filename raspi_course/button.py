import RPi.GPIO as GPIO
import time

BUTTON_PIN = 26

GPIO.setmode(GPIO.BCM)
GPIO.setup(BUTTON_PIN, GPIO.IN) #makes the button pin an input to read data

print(GPIO.input(BUTTON_PIN))

GPIO.cleanup()

exit()

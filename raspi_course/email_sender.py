import yagmail

password = ""

with open("/home/apper/.local/share/.email_password", "r") as f:
    password = f.read()

yag = yagmail.SMTP("andresraspi4@gmail.com", password)

yag.send(to="appermuy@gmail.com",
        subject="first email",
        contents="Hello from my raspi!",
        attachments="/home/apper/raspberrypi_files/raspi_course/test_attachment.txt")
print("email sent")

exit()

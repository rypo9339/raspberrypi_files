import RPi.GPIO as GPIO         #module needed to use GPIO Pins
import time

GPIO.setmode(GPIO.BCM)          #ensures that we are using the correct GPIO number

LED_PIN = 17                    #17 is the GPIO number, NOT THE PIN NUMBER, which is 11

i = 1

GPIO.setup(LED_PIN, GPIO.OUT)   #make the pin output mode

for i in range(1,11):
    print("blink",i)
    GPIO.output(LED_PIN, GPIO.HIGH) #turns the LED on
    time.sleep(1)                   #time delay
    GPIO.output(LED_PIN,GPIO.LOW)   #turns the LED off
    time.sleep(1)
    i += 1

GPIO.cleanup()                  #resets pins after program is done

exit()

import RPi.GPIO as GPIO
import time

LED_PIN = 17
BUTTON_PIN = 26

GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN, GPIO.OUT)
GPIO.setup(BUTTON_PIN, GPIO.IN)

while True:
    if GPIO.input(BUTTON_PIN) == GPIO.HIGH:
        GPIO.output(LED_PIN, GPIO.HIGH)
        time.sleep(0.1)
    else:
        GPIO.output(LED_PIN, GPIO.LOW)
        time.sleep(0.1)

GPIO.cleanup()

exit()

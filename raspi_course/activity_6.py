import RPi.GPIO as GPIO
import time

LED_RED_PIN = 17
LED_GREEN_PIN = 27
LED_BLUE_PIN = 22
BUTTON_PIN = 26

GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_RED_PIN, GPIO.OUT)
GPIO.setup(LED_GREEN_PIN, GPIO.OUT)
GPIO.setup(LED_BLUE_PIN, GPIO.OUT)
GPIO.setup(BUTTON_PIN, GPIO.IN)

GPIO.output(LED_RED_PIN, GPIO.HIGH)
GPIO.output(LED_GREEN_PIN, GPIO.HIGH)
GPIO.output(LED_BLUE_PIN, GPIO.HIGH)

time.sleep(3)

counter = 1

def light_change(counter):
    new_count = 0
    if counter == 1:
        GPIO.output(LED_RED_PIN, GPIO.HIGH)
        GPIO.output(LED_GREEN_PIN, GPIO.LOW)
        GPIO.output(LED_BLUE_PIN, GPIO.LOW)
        new_count = 2
        print("r->g")
        
    elif counter == 2:
        GPIO.output(LED_RED_PIN, GPIO.LOW)
        GPIO.output(LED_GREEN_PIN, GPIO.HIGH)
        GPIO.output(LED_BLUE_PIN, GPIO.LOW)
        new_count = 3
        print("g->b")
        
    elif counter == 3:
        GPIO.output(LED_RED_PIN, GPIO.LOW)
        GPIO.output(LED_GREEN_PIN, GPIO.LOW)
        GPIO.output(LED_BLUE_PIN, GPIO.HIGH)
        new_count = 1
        print("b->r")
    return new_count

old_state = GPIO.input(BUTTON_PIN)

while True:
    time.sleep(0.01)
    button_state = GPIO.input(BUTTON_PIN)
    if button_state != old_state:
        old_state = button_state
        new_light = light_change(counter)
        counter = new_light
        time.sleep(0.3)
        print(counter)
    else:
        time.sleep(0.3)
        print("No Action")

GPIO.cleanup()

exit()



